const schema = require('./validateSchemas/creditCardSchema.js');

export default class ValidateParams {

  constructor(){

  }

  validateNewCreditCard(params){

    const result = schema.newCreditCardSchema.validate({
      card_number: params.card_number,
      card_cvv: params.card_cvv,
      card_expiration: params.card_expiration,
      card_company: params.card_company
    })

    if(result.error === null){
      return true;
    }

    return result;

  }

  validateEditCreditCard(params){

    const result = schema.editCreditCardSchema.validate({
      card_id: params.card_id
    })

    if(this.validateNewCreditCard(params) && result.error === null){
      return true;
    }

    return false;

  }

  validateRemoveCreditCard(params){

    const result = schema.editCreditCardSchema.validate({
      card_id: params.card_id
    })

    if(result.error === null){
      return true;
    }

    return false;

  }


}
