const Joi = require('joi')

const newCreditCardSchema = Joi.object().keys({
  card_number: Joi.string().min(16).max(16).required(),
  card_cvv: Joi.string().min(3).max(3).required(),
  card_expiration: Joi.string().regex(/^[0-9]{2}\/[0-9]{2}$/).required(),
  card_company: Joi.string().min(10).max(100).required()
})

const editCreditCardSchema = Joi.object().keys({
  card_id: Joi.number().integer().min(1).required()
})

module.exports = {
   newCreditCardSchema: newCreditCardSchema,
   editCreditCardSchema: editCreditCardSchema
}
