import ValidadeParams from './ValidateParams.js'
import Utils from '../../utils/utils.js'

var dbFile = require('../config/db.js');
var dbConnection = dbFile();

module.exports = {

  'home':function(request, reply){
     reply('Seja bem vindo a API de acesso, consulte as informações da API antes de começar...');
	},
	'addCreditCard':function(request, reply){

    var validade = new ValidadeParams().validateNewCreditCard(request.payload);

    if(validade == true){

    dbConnection.query(`INSERT INTO user_cards (card_number, card_cvv, card_expiration, card_company) VALUES ('${Utils.encryptText(request.payload.card_number)}','${Utils.encryptText(request.payload.card_cvv)}','${Utils.convertDateToDatabase(request.payload.card_expiration)}','${request.payload.card_company}')`,
    function (error, results) {
      if (error){
        reply({status: 400, error: error})
        throw error
      }
      reply({status: 200, results: results})
   		});

    }else{
      reply({status: 400, error: JSON.stringify(validade)})
    }

	},
  'editCreditCard':function(request, reply){

    var validade = new ValidadeParams().validateEditCreditCard(request.payload);

    if(validade){

      dbConnection.query(`UPDATE user_cards SET card_number = '${Utils.encryptText(request.payload.card_number)}', card_cvv = '${Utils.encryptText(request.payload.card_cvv)}', card_expiration = '${Utils.convertDateToDatabase(request.payload.card_expiration)}', card_company = '${request.payload.card_company}' WHERE id = ${request.payload.card_id}`,
      function (error, results) {
        if (error){
          reply({status: 400, error: error})
          throw error
        }
        reply({status: 200, results: results})
     		});

    }else{
      reply({status: 400, error: JSON.stringify(validade)})
    }

	},
  'removeCreditCard':function(request, reply){

    var validade = new ValidadeParams().validateRemoveCreditCard(request.payload);

    if(validade){
      dbConnection.query(`DELETE FROM user_cards WHERE id = ${ request.payload.card_id }`,
      function (error, results) {
        if (error){
          reply({status: 400, error: error})
          throw error
        }
        reply({status: 200, results: results})
     		});;

    }else{
      reply({status: 400, error: JSON.stringify(validade)})
    }
	},
  'listAllCreditCard':function(request, reply){

    dbConnection.query(`SELECT * FROM user_cards ORDER BY created_date DESC`,
    function (error, results) {
       if (error){
         reply({status: 400, error: error})
         throw error
       }

       results.forEach(function(cards){
         cards.card_number = Utils.decryptText(cards.card_number);
         cards.card_cvv = Utils.decryptText(cards.card_cvv);
       });

       reply(results)
    });

	},
  'listSpecificCreditCard':function(request, reply){

    var validade = new ValidadeParams().validateRemoveCreditCard(request.query);

    if(validade){
      dbConnection.query(`SELECT * FROM user_cards WHERE id = ${request.query.card_id}`,
      function (error, results) {
        if (error){
          reply({status: 400, error: error})
          throw error
        }
        reply(results)
      });

    }else{
      reply({status: 400, error: JSON.stringify(validade)})
    }


  }
}
