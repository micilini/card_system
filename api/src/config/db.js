var mysql = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'system_cards'
});

module.exports = function(){
	return connection;
}
