'use strict';

var Route = require('./routes/routes.js');

const Hapi = require('hapi')

const server = new Hapi.Server();
server.connection({
    host: 'localhost',
    port: 3000
});

//Adiciona as rotas a aplicação

server.route(Route());

const init = async () => {
  
/*
await server.register({
			plugin: require('hapi-pino'),
			options: {
					prettyPrint: true,
					logEvents: ['response']
			}
});
*/

// Start Server
await server.start(err => {
    if (err) {
        // Fancy error handling here
        console.error(err);
        throw err;
    }
    console.log(`Server started at ${ server.info.uri }`);
});

};

init();
