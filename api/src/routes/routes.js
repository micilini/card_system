var CreditCardDAO = require('../models/creditCardDAO.js');

module.exports = function(){
	return [
		{
			method: 'GET',
			path: '/',
			config: {
				handler: CreditCardDAO.home,
				description: 'Mostra uma mensagem inicial da API',
				cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
			}
		},
		{
			method: 'POST',
			path: '/addCreditCard',
			config: {
				handler: CreditCardDAO.addCreditCard,
				description: 'Adiciona um novo cartão de credito no banco de dados',
				cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
			}
		},
		{
			method: 'PUT',
			path: '/editCreditCard',
			config: {
				handler: CreditCardDAO.editCreditCard,
				description: 'Faz a edição de um cartão de credito existente',
				cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
			}
		},
		{
			method: 'DELETE',
			path: '/removeCreditCard',
			config: {
				handler: CreditCardDAO.removeCreditCard,
				description: 'Remove um cartão de credito existente nos registros',
				cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
			}
		},
		{
			method: 'GET',
			path: '/listAllCreditCard',
			config: {
				handler: CreditCardDAO.listAllCreditCard,
				description: 'Lista todos os cartões de creditos existenets nos registros',
				cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
			}
		},
		{
			method: 'GET',
			path: '/listSpecificCreditCard',
			config: {
				handler: CreditCardDAO.listSpecificCreditCard,
				description: 'Lista um cartão de credito especifico informando um card_id',
				cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
			}
		}
	]
}
