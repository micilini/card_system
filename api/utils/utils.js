var CryptoJS = require("crypto-js");

module.exports = {
    convertDateToDatabase: function(stringDate){

      var myDate = '01/' + stringDate;//01/12/21
      var arrMyDate = myDate.split('/');
      var formattedDateString = '20' + arrMyDate[2] + '-' + this.fixMonthProblem(arrMyDate[1]) + '-' +
  arrMyDate[0];

      return formattedDateString;

    },
    encryptText: function(string){
         return CryptoJS.AES.encrypt(string, 'upnid123');
    },
    decryptText: function(string){
      var bytes  = CryptoJS.AES.decrypt(string.toString(), 'upnid123');
      return bytes.toString(CryptoJS.enc.Utf8);
    },
    fixMonthProblem: function(month){
      //TODO: Esta função arruma o problema de meses que não existem, caso o usuario digitar o mês 00 ele vai pra 01 ou caso ele digitar um mês maior que 12 ele vai pra 12. Este problema deve ser arrumado no frontend!
      if(Number(month) == 0){
        return '01';
      }else if(Number(month) > 12){
        return '12';
      }else{
        return month;
      }
    }
}
