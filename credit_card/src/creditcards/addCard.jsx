import React, { Component } from 'react'
import axios from 'axios'
import {browserHistory} from 'react-router';
import InputMask from 'react-input-mask'
import Utils from '../utils/utils'
import Config from '../config/config'

export default class addCard extends Component {

  constructor(props){
    super(props)
    this.state = { card_number: '', card_company: '', card_expiration: '', card_cvv: '', disabled_inputs: false, button_value: 'Criar Cartão' }
    this.handleChange = this.handleChange.bind(this);
    this.submitCard = this.submitCard.bind(this);
	}

  handleChange(event){
    this.setState({ [event.target.name]: event.target.value });
	}

  submitCard(event){
    event.preventDefault();
    this.setState({ button_value: 'Processando...', disabled_inputs: true });

    axios.post(Config.URL_API + '/addCreditCard', {
      card_number: Utils.removeAllWhitesSpaces(this.state.card_number),
      card_cvv: this.state.card_cvv,
      card_expiration: Utils.removeAllWhitesSpaces(this.state.card_expiration),
      card_company: this.state.card_company
    }).then(function(resp){ if(resp.data.status == 200){ browserHistory.push('/') } });

  }

	render(){
		return (
      <form action="#" className="credit-card-div" onSubmit={this.submitCard}>
            <div className="panel panel-default">
              <div className="panel-heading">
                <div className="row ">
                  <div className="col-md-12">
                    <span className="help-block text-muted small-font"> Numero do Cartão</span>
                    <InputMask type="text" name="card_number" className="form-control" onChange={this.handleChange} value={this.state.card_number} placeholder="0000 0000 0000 0000" disabled={this.state.disabled_inputs} mask="9999 9999 9999 9999" maskChar=" " required/>
                  </div>
                </div>
                <div className="row ">
                  <div className="col-md-12 pad-adjust">
                    <span className="help-block text-muted small-font"> Nome da Compania</span>
                    <input type="text" name="card_company" className="form-control" onChange={this.handleChange} value={this.state.card_company} pattern=".{10,100}" required title="10 a 100 caracteres permitidos" placeholder="Upnid INC." disabled={this.state.disabled_inputs}  required/>
                  </div>
                </div>
                <div className="row ">
                  <div className="col-md-3 col-sm-3 col-xs-3">
                    <span className="help-block text-muted small-font"> Data de Expiração</span>
                    <InputMask type="text" name="card_expiration" className="form-control" onChange={this.handleChange} value={this.state.card_expiration} placeholder="MM/YY" disabled={this.state.disabled_inputs} mask="99 / 99" maskChar=" "  required/>
                  </div>
                  <div className="col-md-3 col-sm-3 col-xs-3">
                    <span className="help-block text-muted small-font">Senha CVV</span>
                    <InputMask type="text" name="card_cvv" className="form-control" onChange={this.handleChange} value={this.state.card_cvv} placeholder="CVV" disabled={this.state.disabled_inputs} mask="999" maskChar=" "  required/>
                  </div>
                  <div className="col-md-3 col-sm-3 col-xs-3">
                    <span className="help-block text-muted small-font">Finalizar ?</span>
                    <button type="submit" className="btn btn-success btn-block" disabled={this.state.disabled_inputs}>{this.state.button_value}</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
		)
	}

}
