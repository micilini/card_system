import React, {Component} from 'react'
import axios from 'axios'
import ListCards from './listCards'
import Config from '../config/config'

export default class List extends Component{

  constructor(props){
    super(props)
    this.state = { list: [] }
    this.handleAdd = this.handleAdd.bind(this)
    this.refresh()
  }

  refresh(){
    axios.get(`${Config.URL_API}/listAllCreditCard`).then(resp => this.setState({...this.state, list: resp.data}))
  }

  handleAdd(){
    axios.post(URL, {description}).then(resp => this.refresh())
  }

  render(){
    return(
      <div>
      <ListCards list={this.state.list} />
      </div>
    )
  }

}
