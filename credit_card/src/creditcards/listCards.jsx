import React, { Component } from 'React'
import IconButton from '../template/IconButton'
import axios from 'axios'
import {browserHistory} from 'react-router';
import Utils from '../utils/utils'
import Config from '../config/config'

export default class ListCards extends Component {

  constructor(props){
    super(props)
    this.state = { list: this.props.list }
	}

  deleteSelectedCard(card_id){

    axios.delete(Config.URL_API + '/removeCreditCard', { data: { card_id: card_id } }).then(function(resp){ if(resp.data.status == 200){ location.reload(); } });

  }

  editSelectedCard(card){

browserHistory.push({
  pathname: '/edit',
  state: card
})

  }

  renderRows() {
        const list = this.props.list || []
        return list.map(cards => (
            <tr key={cards.id}>
                <td>
                <strong>**** **** **** {Utils.getFirstFourDigits(cards.card_number)}</strong>
                </td>
                <td>
                {Utils.getMonthAndYear(cards.card_expiration, ' / ')}
                </td>
                <td>
                {cards.card_company}
                </td>
                <td>
                <IconButton styles='success' onClick={() => this.editSelectedCard(cards)} icon='edit'></IconButton>
                <IconButton styles='warning' onClick={() => this.deleteSelectedCard(cards.id)} icon='remove'></IconButton>
                </td>
            </tr>
        ))
    }

  render(){
  return(
    <table className='table'>
            <thead>
                <tr>
                    <th>Numero do Cartão</th>
                    <th>Data de Expiração</th>
                    <th>Nome da Compania</th>
                    <th className='tableActions'>Ações</th>
                </tr>
            </thead>
            <tbody>
                {this.renderRows()}
            </tbody>
        </table>
  )
  }

}
