import React from 'react'
import { Router, Route, Redirect, hashHistory, browserHistory } from 'react-router'

import List from '../creditcards/list'
import AddCard from '../creditcards/addCard'
import editCard from '../creditcards/editCard'

export default props => (
  <Router history={browserHistory}>
  <Route path='/' component={List} />
  <Route path='/add' component={AddCard} />
  <Route path='/edit' component={editCard} />
  <Redirect from='*' to='/' />
  </Router>
)
