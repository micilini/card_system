module.exports = {
    removeAllWhitesSpaces: function(string){
      return string.replace(/\s+/g, '');
    },
    getMonthAndYear: function(date, separator = ''){
      var dt = new Date(date);
      var month = ((dt.getMonth() + 1) < 10 ? '0' : '') + (dt.getMonth() + 1)
      var year = dt.getFullYear().toString().substr(-2);
      return month + separator + year;
    },
    getFirstFourDigits: function(string){
      return string.substr(string.length - 4);
    }
}
