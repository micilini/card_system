import React from 'react'

export default props => (
  <nav className="navbar navbar-inverse">
          <div className="navbar-header">
            <button className="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
            <a className="navbar-brand" href="/"><i className="fa fa-credit-card"></i> Gerenciamento de Cartões</a>
            <a className="navbar-brand" href="/"><i className="fa fa-list"></i> Listar Todos</a>
            <a className="navbar-brand" href="/add"><i className="fa fa-plus"></i> Adicionar Novo</a>
          </div>
   </nav>
)
