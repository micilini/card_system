# Gerenciador de Cartões de Credito [![codecov.io Code Coverage](https://img.shields.io/codecov/c/github/dwyl/hapi-auth-jwt2.svg?maxAge=2592000)](https://codecov.io/github/dwyl/hapi-auth-jwt2?branch=master) [![Build Status](https://travis-ci.org/dwyl/esta.svg?branch=master)](https://travis-ci.org/dwyl/esta)

Este é um projeto de gerenciamento de cartões de credito, sendo possível para o usuário:

* Cadastrar cartão
* Editar cartão
* Remover cartão
* Listar cartões

## Tecnologias Usadas

* React
* Mysql
* NodeJS
* HapiJS
* Redux

## Como executar este projeto ?

Primeiramente certifique-se de que você possui as seguintes tecnologias instaladas no seu sistema operacional:

* NodeJS (https://nodejs.org/en/) (NodeJS 8.11.1) (Npm 5.6.0)
* Mysql Server (https://dev.mysql.com/downloads/mysql/) (Mysql Server 8.0)

## Clonando o Repositorio (Download dos Arquivos)

Após isso, você deverá criar uma nova pasta no seu computador aonde ficará armazenado os arquivos deste projeto, vou fazer isso pelo terminal:
#
```
cd C:/
mkdir card_system
cd ./card_system
```
#
Em seguida basta executar o comando abaixo no terminal para fazer o clone do repositorio para dentro da sua pasta atual:
#
```
git clone https://micilini@bitbucket.org/micilini/card_system.git
```

## Instalando as Dependencias (Arquivos Necessarios)

Chegou a hora de instalar as dependencias do projeto (Porque se não você não vai conseguir executar rs), para isso basta acessar a pasta api e rodar o ```npm i``` da seguinte forma:
#
```
cd ./card_system/api
npm i --save crypto-js@3.1.9-1 hapi@16.1.0 hapi-bodyparser@2.0.2 hapi-pino@4.0.4 joi@13.3.0 moment@2.22.1 mongoose@5.1.1 mysql@2.15.0 nodemon@1.17.4 babel-cli@6.26.0 babel-preset-env@1.7.0
```
#
Observe que estou instalando as versões informadas no comando acima, isso é para garantir que o projeto funcione, mas se voce desejar instalar versões mais novas (O QUE NÃO É MUITO RECOMENDAVEL), basta executar o comando abaixo:
#
```
cd ./card_system/api
npm i --save
```
#
Depois de instalar todas as dependencias nós devemos fazer a mesma coisa na segunda pasta (*credit_card*)
#
```
cd ../credit_card
npm i --save axios@0.15.3 babel-core@6.22.1 babel-loader@6.2.10 babel-plugin-react-html-attrs@2.0.0 babel-plugin-transform-object-rest-spread@6.22.0 babel-preset-es2015@6.22.0 babel-preset-react@6.22.0 bootstrap@3.3.7 css-loader@0.26.1 extract-text-webpack-plugin@1.0.1 file-loader@0.9.0 font-awesome@4.7.0 react@15.4.2 react-dom@15.4.2 react-router@3.0.2 style-loader@0.13.1 webpack@1.14.0 webpack-dev-server@1.16.2 react-input-mask@1.2.2 react-redux@5.0.7 redux@4.0.0
```
#
Novamente... se você desejar instalar as versões mais atualizadas (O QUE NÃO É MUITO RECOMENDAVEL), basta executar:
#
```
cd ../credit_card
npm i --save
```

## Configurando o Banco de Dados

Este projeto faz uso do banco de dados para armazenar as informações dos cartões, se você instalou corretamente o Mysql, basta executar os seguintes passos (A estrutura das pastas do seu computador podem estar diferentes, então cuidado...)
#
```
cd C:\Program Files\MySQL\MySQL Server 8.0\bin
mysql -u root -p 
```
#
O terminal pedirá para que você informe a sua senha do banco de dados (aquela que voce configurou no processo de instalação). Em seguida vamos criar um novo banco de dados e importar o arquivo sql existente dentro da pasta do nosso projeto (Olha que molezinha)
#
```
create database system_cards;
use system_cards;
source C:\card_system\card_system\system_cards.sql
```

## Configurações Finais

Dependencias instaladas e banco de dados configurado, chegou a hora de fazer algumas configurações finais e colocar o nosso projeto pra rodar :D

Dentro da pasta API, localize o arquivo chamado ```db.js``` (No meu caso ele está em ```C:\card_system\card_system\api\src\config```) dentro dele esta as configurações de conexão com o banco de dados, que voce pode modificar de acordo com as suas configurações:
#
```
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',//-> informe o nome de usuario do banco de dados
  password : '',//-> Informe a senha do banco de dados
  database : 'system_cards'
});
```
#
Feito isso, chegou a hora de executar o projeto :D Para isso você vai precisar de dois terminais abertos, um para rodar o backend (pasta ```api```) e o outro para rodar o frontend (pasta ```credit_card```).
#
Com o Terminal 1 aberto digite:
#
```
cd C:\card_system\card_system\api
npm start
```
#
Com o Terminal 2 aberto digite:
#
```
cd C:\card_system\card_system\credit_card
npm start
```

## Executando no navegador

Se você fez todos os passos anteriores e ambos os terminais não informaram nenhum tipo de erro de compilação... Otimo! para testar o projeto basta acessar no navegador o seguinte endereço ```http://localhost:8080```.
#
William Lima, Obrigado :D




